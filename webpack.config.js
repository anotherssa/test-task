const path = require('path');

const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')


const entry = {
    // JS files
    main: path.resolve(__dirname, 'assets/src/js/main.js'),
    // CSS files
    style: path.resolve(__dirname, 'assets/src/scss/main.scss')
};

module.exports = {
    mode: 'development',
    entry,
    output: {
        path: path.resolve(__dirname, 'assets/dist'),
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, 'assets/src/scss'),
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        // Remove the extra JS files Webpack creates for CSS entries.
        // This should be fixed in Webpack 5.
        new FixStyleOnlyEntriesPlugin({
            silent: true,
        }),

        // Clean the `dist` folder on build.
        new CleanWebpackPlugin(),

        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),

        // Run BrowserSync.
        new BrowserSyncPlugin(
            {
                host: 'localhost',
                port: 3000,
                files: [
                    'index.html',
                    'dist/**/*.js',
                    'dist/**/*.css',
                ],
                server: './'
            }
        )
    ]
};