$(function () {
    $('.cards').slick({
        mobileFirst: true,
        prevArrow: '.cards-nav__arrow_left',
        nextArrow: '.cards-nav__arrow_right',
        slidesPerRow: 1,
        rows: 1,
        dots: true,
        appendDots: '.cards-nav__dots',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    rows: 2,
                    slidesPerRow: 2,
                }
            },
            {
                breakpoint: 1149,
                settings: {
                    rows: 2,
                    slidesPerRow: 3
                }
            }
        ]
    });
})